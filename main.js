var express = require('express')
var app = express();
var http = require('http').Server(app);
var mqtt = require('mqtt')
var io = require('socket.io')(http);


app.use('/js', express.static(__dirname+'/js'));

var main_socket = new Object();

var client  = mqtt.connect({ host: 'localhost', port: 1883 })

client.on('connect', function () {
  client.subscribe("device/+/out")
})

app.get('/', function(req, res){
  res.sendFile(__dirname + '/stream.html');
});

io.on('connection', function(socket){
  console.log('a user connected');
  main_socket = socket;
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

  socket.on('sock_device_1_in', function(msg){
    client.publish('device/1/in', msg)
  });

  socket.on('sock_device_2_in', function(msg){
    client.publish('device/2/in', msg)
  });

});

client.on('message', function (topic, message) {

console.log(topic , message.toString());
if(topic == "device/1/out")
	io.emit('sock_device_1_out', message.toString());

if(topic == "device/2/out")
        io.emit('sock_device_2_out', message.toString());

})

http.listen(80, function(){
  console.log('listening on *:80');
});
